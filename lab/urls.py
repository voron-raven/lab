from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import TemplateView
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = patterns('',
    url(r'^$', 'tasks.views.main', name='main'),
    url(r'^task/(\d+)$', 'tasks.views.showtask', name='showtask'),
    url(r'^task/(\d+)/edit$', 'tasks.views.edittask', name='edittask'),
    url(r'^task/(\d+)/delete$', 'tasks.views.deletetask', name='deletetask'),
    url(r'^task/(\d+)/addworklog$', 'tasks.views.addworklog', name='addworklog'),
    url(r'^task/(\d+)/postworklog$', 'tasks.views.postworklog', name='postworklog'),
    url(r'^task/(\d+)/worklogs$', 'tasks.views.worklogs', name='worklogs'),
    url(r'^worklogs$', 'tasks.views.worklogs', name='worklogs'),
    url(r'^projects$', 'tasks.views.projects', name='projects'),
    url(r'^project/(\d+)$', 'tasks.views.showproject', name='showproject'),
    url(r'^project/(\d+)/tasks$', 'tasks.views.showprojecttasks', name='showprojecttasks'),
    url(r'^project/(\d+)/newtask$', 'tasks.views.newtask', name='newtask'),
    # url(r'^createtask$', 'tasks.views.createtask', name='createtask'),
    # url(r'^posting$', 'tasks.views.posting', name='posting'),

    url(r'^login$', 'users.views.login', name='login'),
    url(r'^logout$', 'users.views.logout', name='logout'),
    url(r'^user/(\d+)$', 'users.views.showuser', name='showuser'),
    url(r'^user/(\d+)/statistic$', 'users.views.userstatistic', name='userstatistic'),
    url(r'^map$', 'users.views.map', name='map'),
    
    url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
    urlpatterns +=  static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
