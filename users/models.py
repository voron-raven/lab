from django.db import models
from django.contrib.auth.models import User
import datetime
import os
from django.conf import settings
from django.forms import ModelForm
from django.core.validators import RegexValidator
from django.utils.translation import ugettext as _


class Statistic(models.Model):
    """Statistic model"""
    user = models.ForeignKey(User)
    created_date = models.DateTimeField(auto_now_add=True, default=datetime.date.today())

    def __unicode__(self):
        return '%s - %s' % (
            self.user,
            self.created_date.strftime('%d %B %Y, %H:%M'),
        )

    class Meta:
        ordering = ['-created_date']

class Profile(models.Model):
    """Profile model"""
    STATUS_CHOICES = (
        ('1', _('Single')),
        ('2', _('In a relationship')),
        ('3', _('Engaged')),
        ('4', _('Married')),
        ('5', _("It's complicated")),
        ('6', _('In an open relationship')),
        ('7', _('Widowed')),
        ('8', _('Separated')),
        ('9', _('Divorced'))
    )
    # phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message=_("Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed."))

    user = models.ForeignKey(User, unique=True)
    middle_name = models.CharField(max_length=40, default='exemple', verbose_name=_('Middle name'))
    picture = models.ImageField(upload_to='avatars', default = os.path.join(settings.STATIC_URL,'no-avatar.jpg'), verbose_name=_('picture'))
    birthday = models.DateField(default=datetime.date.today, verbose_name=_('birthday'))
    relationship_status = models.CharField(max_length=10, choices=STATUS_CHOICES, default=1, verbose_name=_('relationship status'))
    skype = models.CharField(max_length=40, default='exemple', verbose_name=_('skype'))
    corporative_skype = models.CharField(max_length=40, default='id.exemple', verbose_name=_('corporative skype'))
    corporative_email = models.EmailField(max_length=40, default='exemple@internetdevels.ua', verbose_name=_('corporative email'))
    phone_number = models.CharField(max_length=15, default='+380123456789', verbose_name=_('phone number'))
    facebook_profile = models.URLField(max_length=60, default='https://facebook.com/exemple', verbose_name=_('facebook profile'))
    map_x_pos = models.PositiveIntegerField(max_length=1625, default=0)
    map_y_pos = models.PositiveIntegerField(max_length=1950, default=0)

    def __unicode__(self):
        return u'Profile of user: %s' % self.user.username

class ProfileForm(ModelForm):
    """ProfileForm model"""
    class Meta:
        model = Profile
        fields = ['picture', 'middle_name', 'birthday', 'relationship_status', 'skype', 'corporative_skype', 'corporative_email', 'phone_number', 'facebook_profile']

class UserForm(ModelForm):
    """UserForm model"""
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email']