from django.db import models
from django.contrib.auth.models import User
from django.forms import ModelForm
import datetime
from django.template.defaultfilters import slugify
from django.utils.translation import ugettext as _

def _get_username(self):
    """return username"""
    return User.objects.get(id = self.author_id)

class Project(models.Model):
    """Project model"""
    name = models.CharField(max_length=60, unique=True)
    note = models.TextField(blank=True, null=True)
    users = models.ManyToManyField(User, related_name='users')
    menegers = models.ManyToManyField(User, related_name='menegers')
    completed = models.BooleanField(default=False)
    created_by = models.ForeignKey(User, related_name='project_created_by', default='')
    created_date = models.DateTimeField(auto_now_add=True, default=datetime.date.today())
    changed_date = models.DateTimeField(auto_now=True, default=datetime.date.today())
    slug = models.SlugField(max_length=60, editable=False)  

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.name)

        super(Project, self).save(*args, **kwargs)

    def __unicode__(self):
        return u'%s' % (
            self.name,
        )        

    # Custom manager lets us do things like Item.completed_tasks.all()
    objects = models.Manager()

    def incomplete_tasks(self):
        # Count all incomplete tasks on the current list instance
        return Project.objects.filter(project=self, status=4)

    class Meta:
        ordering = ["completed", "created_date"]
        verbose_name_plural = "Projects"

class Task(models.Model):
    """Task model"""

    BLOCKER_STATUS = 4
    CRITICAL_STATUS = 3
    MAJOR_STATUS = 2
    MINOR_STATUS = 1
    TRIVIAL_STATUS = 0

    PRIORITY_CHOICES = (
        (BLOCKER_STATUS, _('Blocker')),
        (CRITICAL_STATUS, _('Critical')),
        (MAJOR_STATUS, _('Major')),
        (MINOR_STATUS, _('Minor')),
        (TRIVIAL_STATUS, _('Trivial'))
    )

    REOPENED_STATUS = 1
    AWAITING_STATUS = 2
    RESOLVED_STATUS = 3
    CLOSED_STATUS = 4

    STATUS_CHOICES = (
        (REOPENED_STATUS, _('Reopened')),
        (AWAITING_STATUS, _('Awaiting')),
        (RESOLVED_STATUS, _('Resolved')),
        (CLOSED_STATUS, _('Closed'))
    )

    title = models.CharField(max_length=140, verbose_name=_('title'))
    note = models.TextField(blank=True, null=True, verbose_name=_('note'))
    project = models.ForeignKey(Project)
    created_by = models.ForeignKey(User, related_name='task_created_by', default='', verbose_name=_('created by'))
    assigned_to = models.ForeignKey(User, related_name='assigned_to', verbose_name=_('assigned to'))
    priority = models.PositiveIntegerField(max_length=5, choices=PRIORITY_CHOICES, default=MAJOR_STATUS, verbose_name=_('priority'))
    created_date = models.DateTimeField(auto_now_add=True, default=datetime.date.today(), verbose_name=_('created date'))
    changed_date = models.DateTimeField(auto_now=True, default=datetime.date.today(), verbose_name=_('changed date'))
    status = models.PositiveIntegerField(max_length=4, choices=STATUS_CHOICES, default=REOPENED_STATUS, verbose_name=_('status'))
    slug = models.SlugField(max_length=60, editable=False)
 
    def __unicode__(self):
        return u'%s - %s - %s' % (
            self.title,
            self.project,
            self.priority,
        )

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.title)

        super(Task, self).save(*args, **kwargs)
 
    def get_absolute_url(self):
        return reverse('tasks.views.tasks', args=[self.slug])

    class Meta:
        ordering = ["priority", "project", "-created_date"]

class TaskCreateForm(ModelForm):
    """TaskCreateForm model"""
    class Meta:
        model = Task
        fields = ['title', 'note', 'assigned_to', 'priority']

class TaskEditForm(ModelForm):
    """TaskEditForm model"""
    class Meta:
        model = Task
        fields = ['title', 'note', 'assigned_to', 'priority', 'status']

class Worklog(models.Model):
    """Worklog model"""
    note = models.CharField(max_length=500)
    time_spend = models.TimeField()
    created_by = models.ForeignKey(User, related_name='worklog author', default='')
    created_date = models.DateTimeField(auto_now_add=True, default=datetime.date.today())
    changed_date = models.DateTimeField(auto_now=True, default=datetime.date.today())
    task = models.ForeignKey(Task)
    authorname = property(_get_username)

    def __unicode__(self):
        return '%s - %s - %s' % (
            self.created_by,
            self.task,
            self.created_date.strftime('%d %B %Y, %H:%M'),
        )

    class Meta:
        ordering = ['-created_date']

class WorklogForm(ModelForm):
    """WorklogForm model"""
    class Meta:
        model = Worklog
        fields = ['time_spend', 'note']

class Comment(models.Model):
    """Comment model"""
    body = models.TextField()
    author = models.ForeignKey(User, related_name='comment author', default='')
    created_date = models.DateTimeField(auto_now_add=True, default=datetime.date.today())
    changed_date = models.DateTimeField(auto_now=True, default=datetime.date.today())
    task = models.ForeignKey(Task)
    authorname = property(_get_username)

    def __unicode__(self):
        return '%s - %s - %s' % (
            self.author,
            self.task,
            self.created_date.strftime('%d %B %Y, %H:%M'),
        )

    class Meta:
        ordering = ['-created_date']
    
class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = ['body']
